EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 5
Title "JK Headphone Amp"
Date "2020-08-24"
Rev "2"
Comp "Equalizer/Tone Control Section"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 2650 1950
Wire Wire Line
	3400 1950 2650 1950
Wire Wire Line
	3400 2050 3400 1950
Wire Wire Line
	2650 2850 2650 2950
Connection ~ 2650 2850
Wire Wire Line
	3400 2850 3400 2800
Wire Wire Line
	2650 2850 3400 2850
Wire Wire Line
	2650 2350 2650 2850
Wire Wire Line
	2650 1950 2650 2050
Wire Wire Line
	2250 1950 2650 1950
$Comp
L Device:C C?
U 1 1 5F47B81B
P 2650 2200
AR Path="/5F47B81B" Ref="C?"  Part="1" 
AR Path="/5F5B77DA/5F47B81B" Ref="C?"  Part="1" 
AR Path="/5F7EC12B/5F47B81B" Ref="C7"  Part="1" 
F 0 "C7" H 2765 2246 50  0000 L CNN
F 1 "100n" H 2765 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2688 2050 50  0001 C CNN
F 3 "~" H 2650 2200 50  0001 C CNN
	1    2650 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5F47ECEC
P 2650 2950
AR Path="/5F47ECEC" Ref="#PWR?"  Part="1" 
AR Path="/5F5B77DA/5F47ECEC" Ref="#PWR?"  Part="1" 
AR Path="/5F7EC12B/5F47ECEC" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 2650 2700 50  0001 C CNN
F 1 "GNDREF" H 2655 2777 50  0000 C CNN
F 2 "" H 2650 2950 50  0001 C CNN
F 3 "" H 2650 2950 50  0001 C CNN
	1    2650 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 5F47B2D4
P 2100 1950
F 0 "R15" V 2307 1950 50  0000 C CNN
F 1 "4.7k" V 2216 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2030 1950 50  0001 C CNN
F 3 "~" H 2100 1950 50  0001 C CNN
	1    2100 1950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV?
U 1 1 5F5BB969
P 3400 2200
AR Path="/5F5BB969" Ref="RV?"  Part="1" 
AR Path="/5F5B77DA/5F5BB969" Ref="RV?"  Part="1" 
AR Path="/5F7EC12B/5F5BB969" Ref="RV2"  Part="1" 
F 0 "RV2" H 3330 2246 50  0000 R CNN
F 1 "10k" H 3330 2155 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 3400 2200 50  0001 C CNN
F 3 "~" H 3400 2200 50  0001 C CNN
	1    3400 2200
	1    0    0    -1  
$EndComp
Text Notes 3500 2050 0    50   ~ 0
Bass Ctl
Wire Wire Line
	3550 2200 4350 2200
$Comp
L Device:R R17
U 1 1 5F484212
P 3400 2650
F 0 "R17" V 3607 2650 50  0000 C CNN
F 1 "10k" V 3516 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3330 2650 50  0001 C CNN
F 3 "~" H 3400 2650 50  0001 C CNN
	1    3400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2500 3400 2350
Wire Wire Line
	5300 2200 5300 1550
Text GLabel 1600 1550 0    50   Input ~ 0
Left_Bulk_Highpass
Text GLabel 5650 1550 2    50   Input ~ 0
Tone_Left_Out
Wire Wire Line
	5650 1550 5300 1550
Wire Wire Line
	1600 1550 4350 1550
Connection ~ 5300 1550
Text GLabel 1600 1950 0    50   Input ~ 0
Stage1_Left_Out
Wire Wire Line
	1600 1950 1950 1950
Text GLabel 1600 3900 0    50   Input ~ 0
Stage1_Right_Out
Text GLabel 5500 3500 2    50   Input ~ 0
Tone_Right_Out
$Comp
L Device:R_POT_Dual_Separate RV?
U 2 1 5F5BB96F
P 3600 4150
AR Path="/5F5BB96F" Ref="RV?"  Part="1" 
AR Path="/5F5B77DA/5F5BB96F" Ref="RV?"  Part="2" 
AR Path="/5F7EC12B/5F5BB96F" Ref="RV2"  Part="2" 
F 0 "RV2" H 3530 4196 50  0000 R CNN
F 1 "10k" H 3530 4105 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 3600 4150 50  0001 C CNN
F 3 "~" H 3600 4150 50  0001 C CNN
	2    3600 4150
	1    0    0    -1  
$EndComp
Connection ~ 2850 3900
Wire Wire Line
	3600 3900 2850 3900
Wire Wire Line
	3600 4000 3600 3900
Wire Wire Line
	2850 4700 2850 4800
Connection ~ 2850 4700
Wire Wire Line
	3600 4700 3600 4650
Wire Wire Line
	2850 4700 3600 4700
Wire Wire Line
	2850 4300 2850 4700
Wire Wire Line
	2850 3900 2850 4000
Wire Wire Line
	2450 3900 2850 3900
$Comp
L Device:C C?
U 1 1 5F4D3B58
P 2850 4150
AR Path="/5F4D3B58" Ref="C?"  Part="1" 
AR Path="/5F5B77DA/5F4D3B58" Ref="C?"  Part="1" 
AR Path="/5F7EC12B/5F4D3B58" Ref="C8"  Part="1" 
F 0 "C8" H 2965 4196 50  0000 L CNN
F 1 "100n" H 2965 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2888 4000 50  0001 C CNN
F 3 "~" H 2850 4150 50  0001 C CNN
	1    2850 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5F4D3B79
P 2850 4800
AR Path="/5F4D3B79" Ref="#PWR?"  Part="1" 
AR Path="/5F5B77DA/5F4D3B79" Ref="#PWR?"  Part="1" 
AR Path="/5F7EC12B/5F4D3B79" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 2850 4550 50  0001 C CNN
F 1 "GNDREF" H 2855 4627 50  0000 C CNN
F 2 "" H 2850 4800 50  0001 C CNN
F 3 "" H 2850 4800 50  0001 C CNN
	1    2850 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5F4D3B97
P 2300 3900
F 0 "R19" V 2507 3900 50  0000 C CNN
F 1 "4.7k" V 2416 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2230 3900 50  0001 C CNN
F 3 "~" H 2300 3900 50  0001 C CNN
	1    2300 3900
	0    -1   -1   0   
$EndComp
Text Notes 3700 3850 0    50   ~ 0
Bass Ctl
Text GLabel 1600 3500 0    50   Input ~ 0
Right_Bulk_Highpass
Wire Wire Line
	5200 4150 5200 3500
Wire Wire Line
	5200 3500 5500 3500
Wire Wire Line
	1600 3900 2150 3900
$Comp
L Device:R R21
U 1 1 5F479AFF
P 3600 4500
F 0 "R21" V 3807 4500 50  0000 C CNN
F 1 "10k" V 3716 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3530 4500 50  0001 C CNN
F 3 "~" H 3600 4500 50  0001 C CNN
	1    3600 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4350 3600 4300
Connection ~ 5200 3500
$Comp
L Device:R R14
U 1 1 5F47B92B
P 4500 1550
F 0 "R14" V 4707 1550 50  0000 C CNN
F 1 "1k" V 4616 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 1550 50  0001 C CNN
F 3 "~" H 4500 1550 50  0001 C CNN
	1    4500 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 1550 5300 1550
$Comp
L Device:R R16
U 1 1 5F47DEFD
P 4500 2200
F 0 "R16" V 4707 2200 50  0000 C CNN
F 1 "1k" V 4616 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 2200 50  0001 C CNN
F 3 "~" H 4500 2200 50  0001 C CNN
	1    4500 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 2200 5300 2200
Wire Wire Line
	3750 4150 4350 4150
Wire Wire Line
	1600 3500 4350 3500
$Comp
L Device:R R18
U 1 1 5F4853F7
P 4500 3500
F 0 "R18" V 4707 3500 50  0000 C CNN
F 1 "1k" V 4616 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 3500 50  0001 C CNN
F 3 "~" H 4500 3500 50  0001 C CNN
	1    4500 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 3500 5200 3500
$Comp
L Device:R R20
U 1 1 5F485911
P 4500 4150
F 0 "R20" V 4707 4150 50  0000 C CNN
F 1 "1k" V 4616 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4430 4150 50  0001 C CNN
F 3 "~" H 4500 4150 50  0001 C CNN
	1    4500 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 4150 5200 4150
$EndSCHEMATC
