EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 5
Title "JK Headphone Amp"
Date ""
Rev "2"
Comp "Line Out Section"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5F646CA1
P 4800 3400
AR Path="/5F646CA1" Ref="R?"  Part="1" 
AR Path="/5F600C65/5F646CA1" Ref="R11"  Part="1" 
F 0 "R11" H 4870 3446 50  0000 L CNN
F 1 "1k" H 4870 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4730 3400 50  0001 C CNN
F 3 "~" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA2134 U?
U 1 1 5F646CA7
P 5350 2650
AR Path="/5F646CA7" Ref="U?"  Part="1" 
AR Path="/5F600C65/5F646CA7" Ref="U3"  Part="1" 
F 0 "U3" H 5350 3017 50  0000 C CNN
F 1 "OPA2134" H 5350 2926 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5350 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 5350 2650 50  0001 C CNN
	1    5350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2650 5700 2650
$Comp
L power:GNDREF #PWR?
U 1 1 5F646CAE
P 4800 3600
AR Path="/5F646CAE" Ref="#PWR?"  Part="1" 
AR Path="/5F600C65/5F646CAE" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 4800 3350 50  0001 C CNN
F 1 "GNDREF" H 4805 3427 50  0000 C CNN
F 2 "" H 4800 3600 50  0001 C CNN
F 3 "" H 4800 3600 50  0001 C CNN
	1    4800 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3600 4800 3550
Connection ~ 5700 2650
Text GLabel 4650 2550 0    50   Input ~ 0
Tone_Left_Out
Text GLabel 4900 4000 0    50   Input ~ 0
Tone_Right_Out
Wire Wire Line
	4650 2550 5050 2550
$Comp
L Device:R R?
U 1 1 5F646CB9
P 5250 3100
AR Path="/5F646CB9" Ref="R?"  Part="1" 
AR Path="/5F600C65/5F646CB9" Ref="R10"  Part="1" 
F 0 "R10" H 5320 3146 50  0000 L CNN
F 1 "4.7k" H 5320 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5180 3100 50  0001 C CNN
F 3 "~" H 5250 3100 50  0001 C CNN
	1    5250 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 3100 5700 3100
Wire Wire Line
	5700 2650 5700 3100
Wire Wire Line
	5100 3100 4800 3100
Wire Wire Line
	4800 3100 4800 3250
Wire Wire Line
	4800 3100 4800 2750
Wire Wire Line
	4800 2750 5050 2750
Connection ~ 4800 3100
Wire Wire Line
	5700 2650 6450 2650
$Comp
L Device:R R?
U 1 1 5F646CC7
P 4800 4850
AR Path="/5F646CC7" Ref="R?"  Part="1" 
AR Path="/5F600C65/5F646CC7" Ref="R13"  Part="1" 
F 0 "R13" H 4870 4896 50  0000 L CNN
F 1 "1k" H 4870 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4730 4850 50  0001 C CNN
F 3 "~" H 4800 4850 50  0001 C CNN
	1    4800 4850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA2134 U?
U 2 1 5F646CCD
P 5350 4100
AR Path="/5F646CCD" Ref="U?"  Part="2" 
AR Path="/5F600C65/5F646CCD" Ref="U3"  Part="2" 
F 0 "U3" H 5350 4467 50  0000 C CNN
F 1 "OPA2134" H 5350 4376 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5350 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 5350 4100 50  0001 C CNN
	2    5350 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4100 5700 4100
$Comp
L power:GNDREF #PWR?
U 1 1 5F646CD4
P 4800 5050
AR Path="/5F646CD4" Ref="#PWR?"  Part="1" 
AR Path="/5F600C65/5F646CD4" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 4800 4800 50  0001 C CNN
F 1 "GNDREF" H 4805 4877 50  0000 C CNN
F 2 "" H 4800 5050 50  0001 C CNN
F 3 "" H 4800 5050 50  0001 C CNN
	1    4800 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5050 4800 5000
Connection ~ 5700 4100
$Comp
L Device:R R?
U 1 1 5F646CDC
P 5250 4550
AR Path="/5F646CDC" Ref="R?"  Part="1" 
AR Path="/5F600C65/5F646CDC" Ref="R12"  Part="1" 
F 0 "R12" H 5320 4596 50  0000 L CNN
F 1 "4.7k" H 5320 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5180 4550 50  0001 C CNN
F 3 "~" H 5250 4550 50  0001 C CNN
	1    5250 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 4550 5700 4550
Wire Wire Line
	5700 4100 5700 4550
Wire Wire Line
	5100 4550 4800 4550
Wire Wire Line
	4800 4550 4800 4700
Wire Wire Line
	4800 4550 4800 4200
Wire Wire Line
	4800 4200 5050 4200
Connection ~ 4800 4550
Wire Wire Line
	5700 4100 6000 4100
Wire Wire Line
	4900 4000 5050 4000
Wire Wire Line
	6450 2750 6000 2750
Wire Wire Line
	6000 2750 6000 4100
$Comp
L power:GNDREF #PWR?
U 1 1 5F646CED
P 6350 3000
AR Path="/5F646CED" Ref="#PWR?"  Part="1" 
AR Path="/5F600C65/5F646CED" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 6350 2750 50  0001 C CNN
F 1 "GNDREF" H 6355 2827 50  0000 C CNN
F 2 "" H 6350 3000 50  0001 C CNN
F 3 "" H 6350 3000 50  0001 C CNN
	1    6350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2850 6350 2850
Wire Wire Line
	6350 2850 6350 3000
$Comp
L Connector:AudioJack3 J?
U 1 1 5F678DEB
P 6650 2750
AR Path="/5F678DEB" Ref="J?"  Part="1" 
AR Path="/5F600508/5F678DEB" Ref="J?"  Part="1" 
AR Path="/5F600C65/5F678DEB" Ref="J4"  Part="1" 
F 0 "J4" H 6370 2775 50  0000 R CNN
F 1 "LineOut" H 6370 2684 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 6650 2750 50  0001 C CNN
F 3 "~" H 6650 2750 50  0001 C CNN
	1    6650 2750
	-1   0    0    -1  
$EndComp
Text Notes 5300 1950 0    79   ~ 0
Stereo Gain x11
$EndSCHEMATC
