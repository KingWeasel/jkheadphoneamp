EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "JK Headphone Amp"
Date "2020-09-06"
Rev "4"
Comp "Headphone Amp with Treb Cut"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 5F41CC10
P 7050 1000
F 0 "H1" H 7150 1046 50  0000 L CNN
F 1 "MountingHole" H 7150 955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 7050 1000 50  0001 C CNN
F 3 "~" H 7050 1000 50  0001 C CNN
	1    7050 1000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F41E8EA
P 7050 1300
F 0 "H2" H 7150 1346 50  0000 L CNN
F 1 "MountingHole" H 7150 1255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 7050 1300 50  0001 C CNN
F 3 "~" H 7050 1300 50  0001 C CNN
	1    7050 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F41EADD
P 7050 1550
F 0 "H3" H 7150 1596 50  0000 L CNN
F 1 "MountingHole" H 7150 1505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 7050 1550 50  0001 C CNN
F 3 "~" H 7050 1550 50  0001 C CNN
	1    7050 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F41ECC9
P 7050 1800
F 0 "H4" H 7150 1846 50  0000 L CNN
F 1 "MountingHole" H 7150 1755 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 7050 1800 50  0001 C CNN
F 3 "~" H 7050 1800 50  0001 C CNN
	1    7050 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F55B009
P 3550 1150
AR Path="/5F55B009" Ref="R1"  Part="1" 
AR Path="/5F5FFDB8/5F55B009" Ref="R?"  Part="1" 
F 0 "R1" H 3620 1196 50  0000 L CNN
F 1 "10k" H 3620 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3480 1150 50  0001 C CNN
F 3 "~" H 3550 1150 50  0001 C CNN
	1    3550 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 900  3550 1000
$Comp
L Device:C C1
U 1 1 5F55B010
P 1600 1350
AR Path="/5F55B010" Ref="C1"  Part="1" 
AR Path="/5F5FFDB8/5F55B010" Ref="C?"  Part="1" 
F 0 "C1" H 1715 1396 50  0000 L CNN
F 1 "1u" H 1715 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1638 1200 50  0001 C CNN
F 3 "~" H 1600 1350 50  0001 C CNN
	1    1600 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F55B016
P 3900 1600
AR Path="/5F55B016" Ref="R3"  Part="1" 
AR Path="/5F5FFDB8/5F55B016" Ref="R?"  Part="1" 
F 0 "R3" H 3970 1646 50  0000 L CNN
F 1 "10k" H 3970 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3830 1600 50  0001 C CNN
F 3 "~" H 3900 1600 50  0001 C CNN
	1    3900 1600
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7812 U?
U 1 1 5F55B01C
P 2000 900
AR Path="/5F5FFDB8/5F55B01C" Ref="U?"  Part="1" 
AR Path="/5F55B01C" Ref="U1"  Part="1" 
F 0 "U1" H 2000 1142 50  0000 C CNN
F 1 "L7812" H 2000 1051 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2025 750 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2000 850 50  0001 C CNN
	1    2000 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 900  1600 900 
Wire Wire Line
	1600 1200 1600 900 
Wire Wire Line
	1600 1500 1600 1850
$Comp
L Device:C C2
U 1 1 5F55B025
P 2350 1350
AR Path="/5F55B025" Ref="C2"  Part="1" 
AR Path="/5F5FFDB8/5F55B025" Ref="C?"  Part="1" 
F 0 "C2" H 2465 1396 50  0000 L CNN
F 1 "100n" H 2465 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2388 1200 50  0001 C CNN
F 3 "~" H 2350 1350 50  0001 C CNN
	1    2350 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1200 2000 1850
Connection ~ 2000 1850
Wire Wire Line
	2300 900  2350 900 
Wire Wire Line
	2350 900  2350 1200
Wire Wire Line
	2350 1500 2350 1850
Wire Wire Line
	2000 1850 2350 1850
Connection ~ 1600 900 
Connection ~ 1600 1850
Wire Wire Line
	1600 1850 2000 1850
Connection ~ 2350 900 
Connection ~ 2350 1850
Wire Wire Line
	2350 900  3550 900 
Connection ~ 3900 900 
Wire Wire Line
	3900 1750 3900 1850
Connection ~ 3900 1850
Connection ~ 4250 1850
Wire Wire Line
	5500 900  5500 1000
Wire Wire Line
	5500 1600 5500 1850
$Comp
L Amplifier_Operational:OPA2134 U2
U 3 1 5F55B048
P 5600 1300
AR Path="/5F55B048" Ref="U2"  Part="3" 
AR Path="/5F5FFDB8/5F55B048" Ref="U?"  Part="3" 
F 0 "U2" H 5558 1346 50  0000 L CNN
F 1 "OPA2134" H 5558 1255 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5600 1300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 5600 1300 50  0001 C CNN
	3    5600 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1950 4250 1850
Connection ~ 4250 900 
Wire Wire Line
	4250 900  4250 800 
$Comp
L power:VEE #PWR03
U 1 1 5F55B051
P 4250 1950
AR Path="/5F55B051" Ref="#PWR03"  Part="1" 
AR Path="/5F5FFDB8/5F55B051" Ref="#PWR?"  Part="1" 
F 0 "#PWR03" H 4250 1800 50  0001 C CNN
F 1 "VEE" H 4265 2123 50  0000 C CNN
F 2 "" H 4250 1950 50  0001 C CNN
F 3 "" H 4250 1950 50  0001 C CNN
	1    4250 1950
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5F55B057
P 4250 800
AR Path="/5F55B057" Ref="#PWR01"  Part="1" 
AR Path="/5F5FFDB8/5F55B057" Ref="#PWR?"  Part="1" 
F 0 "#PWR01" H 4250 650 50  0001 C CNN
F 1 "VCC" H 4265 973 50  0000 C CNN
F 2 "" H 4250 800 50  0001 C CNN
F 3 "" H 4250 800 50  0001 C CNN
	1    4250 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1000 3900 900 
$Comp
L Device:R R2
U 1 1 5F55B05E
P 3900 1150
AR Path="/5F55B05E" Ref="R2"  Part="1" 
AR Path="/5F5FFDB8/5F55B05E" Ref="R?"  Part="1" 
F 0 "R2" H 3970 1196 50  0000 L CNN
F 1 "10k" H 3970 1105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3830 1150 50  0001 C CNN
F 3 "~" H 3900 1150 50  0001 C CNN
	1    3900 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1850 4250 1850
Wire Wire Line
	3900 900  4250 900 
Connection ~ 3550 900 
Wire Wire Line
	3550 900  3900 900 
Wire Wire Line
	1400 1850 1600 1850
Wire Wire Line
	2350 1850 3550 1850
Connection ~ 3550 1850
Wire Wire Line
	3550 1850 3900 1850
Wire Wire Line
	3900 1300 3900 1400
Wire Wire Line
	4250 900  4550 900 
$Comp
L power:GNDREF #PWR02
U 1 1 5F55B06F
P 5100 1450
AR Path="/5F55B06F" Ref="#PWR02"  Part="1" 
AR Path="/5F5FFDB8/5F55B06F" Ref="#PWR?"  Part="1" 
F 0 "#PWR02" H 5100 1200 50  0001 C CNN
F 1 "GNDREF" H 5105 1277 50  0000 C CNN
F 2 "" H 5100 1450 50  0001 C CNN
F 3 "" H 5100 1450 50  0001 C CNN
	1    5100 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 900  4550 1000
Wire Wire Line
	4550 1750 4550 1850
Connection ~ 4550 900 
Connection ~ 4550 1850
Wire Wire Line
	4550 1850 4250 1850
Wire Wire Line
	5100 1450 5100 1400
Connection ~ 3900 1400
Wire Wire Line
	3900 1400 3900 1450
Wire Wire Line
	4550 1300 4550 1400
Connection ~ 4550 1400
Wire Wire Line
	4550 1400 3900 1400
Wire Wire Line
	4550 1400 4550 1450
Wire Wire Line
	4550 1850 5500 1850
Wire Wire Line
	4550 900  5500 900 
Wire Wire Line
	4550 1400 5100 1400
$Comp
L Device:CP1 C?
U 1 1 5F55B084
P 4550 1150
AR Path="/5F5FFDB8/5F55B084" Ref="C?"  Part="1" 
AR Path="/5F55B084" Ref="C3"  Part="1" 
F 0 "C3" H 4665 1196 50  0000 L CNN
F 1 "470u" H 4665 1105 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P5.00mm" H 4550 1150 50  0001 C CNN
F 3 "~" H 4550 1150 50  0001 C CNN
	1    4550 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5F55B08A
P 4550 1600
AR Path="/5F5FFDB8/5F55B08A" Ref="C?"  Part="1" 
AR Path="/5F55B08A" Ref="C4"  Part="1" 
F 0 "C4" H 4665 1646 50  0000 L CNN
F 1 "470u" H 4665 1555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P5.00mm" H 4550 1600 50  0001 C CNN
F 3 "~" H 4550 1600 50  0001 C CNN
	1    4550 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 900  1600 900 
Wire Wire Line
	1250 1000 1400 1000
Wire Wire Line
	1400 1000 1400 1850
$Comp
L Amplifier_Operational:OPA2134 U2
U 1 1 5F570669
P 4900 3400
AR Path="/5F570669" Ref="U2"  Part="1" 
AR Path="/5F600508/5F570669" Ref="U?"  Part="1" 
F 0 "U2" H 4900 3767 50  0000 C CNN
F 1 "OPA2134" H 4900 3676 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 4900 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 4900 3400 50  0001 C CNN
	1    4900 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3500 4600 3500
Wire Wire Line
	5200 3400 5500 3400
$Comp
L power:GNDREF #PWR05
U 1 1 5F570671
P 3850 3950
AR Path="/5F570671" Ref="#PWR05"  Part="1" 
AR Path="/5F600508/5F570671" Ref="#PWR?"  Part="1" 
F 0 "#PWR05" H 3850 3700 50  0001 C CNN
F 1 "GNDREF" H 3855 3777 50  0000 C CNN
F 2 "" H 3850 3950 50  0001 C CNN
F 3 "" H 3850 3950 50  0001 C CNN
	1    3850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3950 3850 3900
$Comp
L Device:C C7
U 1 1 5F57067B
P 3500 5100
AR Path="/5F57067B" Ref="C7"  Part="1" 
AR Path="/5F600508/5F57067B" Ref="C?"  Part="1" 
F 0 "C7" H 3615 5146 50  0000 L CNN
F 1 "1u" H 3615 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3538 4950 50  0001 C CNN
F 3 "~" H 3500 5100 50  0001 C CNN
	1    3500 5100
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5F570681
P 3850 5350
AR Path="/5F570681" Ref="R7"  Part="1" 
AR Path="/5F600508/5F570681" Ref="R?"  Part="1" 
F 0 "R7" H 3920 5396 50  0000 L CNN
F 1 "100k" H 3920 5305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3780 5350 50  0001 C CNN
F 3 "~" H 3850 5350 50  0001 C CNN
	1    3850 5350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA2134 U2
U 2 1 5F570687
P 4850 5200
AR Path="/5F570687" Ref="U2"  Part="2" 
AR Path="/5F600508/5F570687" Ref="U?"  Part="2" 
F 0 "U2" H 4850 5567 50  0000 C CNN
F 1 "OPA2134" H 4850 5476 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 4850 5200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 4850 5200 50  0001 C CNN
	2    4850 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 5200 3850 5100
Wire Wire Line
	4500 5300 4550 5300
Wire Wire Line
	5150 5200 5700 5200
$Comp
L power:GNDREF #PWR06
U 1 1 5F570690
P 3850 5800
AR Path="/5F570690" Ref="#PWR06"  Part="1" 
AR Path="/5F600508/5F570690" Ref="#PWR?"  Part="1" 
F 0 "#PWR06" H 3850 5550 50  0001 C CNN
F 1 "GNDREF" H 3855 5627 50  0000 C CNN
F 2 "" H 3850 5800 50  0001 C CNN
F 3 "" H 3850 5800 50  0001 C CNN
	1    3850 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 5100 4550 5100
Wire Wire Line
	3650 5100 3850 5100
Connection ~ 3850 5100
Wire Wire Line
	3850 5500 3850 5700
Wire Wire Line
	3150 3150 3150 3000
Wire Wire Line
	2250 5700 2250 3200
Wire Wire Line
	2250 5700 2600 5700
Connection ~ 2600 5700
Wire Wire Line
	2600 5250 2600 5700
Wire Wire Line
	3300 3300 3350 3300
Wire Wire Line
	2750 5100 3350 5100
Wire Wire Line
	3150 3900 3850 3900
Wire Wire Line
	3150 3450 3150 3900
Connection ~ 3850 3900
Wire Wire Line
	3850 5700 2600 5700
Connection ~ 3850 5700
Wire Wire Line
	3850 5700 3850 5800
$Comp
L Device:R R5
U 1 1 5F5706C3
P 4200 3900
AR Path="/5F5706C3" Ref="R5"  Part="1" 
AR Path="/5F600508/5F5706C3" Ref="R?"  Part="1" 
F 0 "R5" H 4270 3946 50  0000 L CNN
F 1 "1k" H 4270 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4130 3900 50  0001 C CNN
F 3 "~" H 4200 3900 50  0001 C CNN
	1    4200 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5F5706C9
P 5150 3900
AR Path="/5F5706C9" Ref="R6"  Part="1" 
AR Path="/5F600508/5F5706C9" Ref="R?"  Part="1" 
F 0 "R6" H 5220 3946 50  0000 L CNN
F 1 "4.7k" H 5220 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5080 3900 50  0001 C CNN
F 3 "~" H 5150 3900 50  0001 C CNN
	1    5150 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 3900 5500 3900
Wire Wire Line
	5500 3400 5500 3900
Wire Wire Line
	5000 3900 4550 3900
Wire Wire Line
	4550 3500 4550 3900
$Comp
L Device:R R8
U 1 1 5F5706D7
P 4150 5700
AR Path="/5F5706D7" Ref="R8"  Part="1" 
AR Path="/5F600508/5F5706D7" Ref="R?"  Part="1" 
F 0 "R8" H 4220 5746 50  0000 L CNN
F 1 "1k" H 4220 5655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4080 5700 50  0001 C CNN
F 3 "~" H 4150 5700 50  0001 C CNN
	1    4150 5700
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5F5706DD
P 5150 5700
AR Path="/5F5706DD" Ref="R9"  Part="1" 
AR Path="/5F600508/5F5706DD" Ref="R?"  Part="1" 
F 0 "R9" H 5220 5746 50  0000 L CNN
F 1 "4.7k" H 5220 5655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5080 5700 50  0001 C CNN
F 3 "~" H 5150 5700 50  0001 C CNN
	1    5150 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 5700 4500 5700
Connection ~ 4500 5700
Wire Wire Line
	4500 5300 4500 5700
Wire Wire Line
	5700 5700 5300 5700
Wire Wire Line
	5700 5200 5700 5700
Wire Wire Line
	3650 3300 3850 3300
Wire Wire Line
	3850 3300 4600 3300
Connection ~ 3850 3300
Wire Wire Line
	3850 3400 3850 3300
$Comp
L Device:R R4
U 1 1 5F5706F0
P 3850 3550
AR Path="/5F5706F0" Ref="R4"  Part="1" 
AR Path="/5F600508/5F5706F0" Ref="R?"  Part="1" 
F 0 "R4" H 3920 3596 50  0000 L CNN
F 1 "100k" H 3920 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3780 3550 50  0001 C CNN
F 3 "~" H 3850 3550 50  0001 C CNN
	1    3850 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5F5706F6
P 3500 3300
AR Path="/5F5706F6" Ref="C5"  Part="1" 
AR Path="/5F600508/5F5706F6" Ref="C?"  Part="1" 
F 0 "C5" H 3615 3346 50  0000 L CNN
F 1 "1u" H 3615 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3538 3150 50  0001 C CNN
F 3 "~" H 3500 3300 50  0001 C CNN
	1    3500 3300
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5F57ADEA
P 5150 4350
AR Path="/5F57ADEA" Ref="C6"  Part="1" 
AR Path="/5F5B77DA/5F57ADEA" Ref="C?"  Part="1" 
AR Path="/5F7EC12B/5F57ADEA" Ref="C?"  Part="1" 
F 0 "C6" H 5265 4396 50  0000 L CNN
F 1 "1u" H 5265 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5188 4200 50  0001 C CNN
F 3 "~" H 5150 4350 50  0001 C CNN
	1    5150 4350
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV2
U 1 1 5F57ADFC
P 4550 4350
AR Path="/5F57ADFC" Ref="RV2"  Part="1" 
AR Path="/5F5B77DA/5F57ADFC" Ref="RV?"  Part="1" 
AR Path="/5F7EC12B/5F57ADFC" Ref="RV?"  Part="1" 
F 0 "RV2" H 4480 4396 50  0000 R CNN
F 1 "TREB" H 4480 4305 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 4550 4350 50  0001 C CNN
F 3 "~" H 4550 4350 50  0001 C CNN
	1    4550 4350
	0    1    -1   0   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV2
U 2 1 5F57AE15
P 4500 6100
AR Path="/5F57AE15" Ref="RV2"  Part="2" 
AR Path="/5F5B77DA/5F57AE15" Ref="RV?"  Part="2" 
AR Path="/5F7EC12B/5F57AE15" Ref="RV?"  Part="2" 
F 0 "RV2" H 4430 6146 50  0000 R CNN
F 1 "TREB" H 4430 6055 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 4500 6100 50  0001 C CNN
F 3 "~" H 4500 6100 50  0001 C CNN
	2    4500 6100
	0    1    -1   0   
$EndComp
$Comp
L Device:C C8
U 1 1 5F57AE25
P 5150 6100
AR Path="/5F57AE25" Ref="C8"  Part="1" 
AR Path="/5F5B77DA/5F57AE25" Ref="C?"  Part="1" 
AR Path="/5F7EC12B/5F57AE25" Ref="C?"  Part="1" 
F 0 "C8" H 5265 6146 50  0000 L CNN
F 1 "1u" H 5265 6055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5188 5950 50  0001 C CNN
F 3 "~" H 5150 6100 50  0001 C CNN
	1    5150 6100
	0    1    1    0   
$EndComp
$Comp
L Connector:AudioJack3 J4
U 1 1 5F587172
P 6500 3500
AR Path="/5F587172" Ref="J4"  Part="1" 
AR Path="/5F600508/5F587172" Ref="J?"  Part="1" 
AR Path="/5F600C65/5F587172" Ref="J?"  Part="1" 
F 0 "J4" H 6220 3525 50  0000 R CNN
F 1 "LineOut" H 6220 3434 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 6500 3500 50  0001 C CNN
F 3 "~" H 6500 3500 50  0001 C CNN
	1    6500 3500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV1
U 2 1 5F5706A9
P 2600 5100
AR Path="/5F5706A9" Ref="RV1"  Part="2" 
AR Path="/5F600508/5F5706A9" Ref="RV?"  Part="2" 
F 0 "RV1" H 2530 5146 50  0000 R CNN
F 1 "VOL" H 2530 5055 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 2600 5100 50  0001 C CNN
F 3 "~" H 2600 5100 50  0001 C CNN
	2    2600 5100
	1    0    0    1   
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV1
U 1 1 5F5706A3
P 3150 3300
AR Path="/5F5706A3" Ref="RV1"  Part="1" 
AR Path="/5F600508/5F5706A3" Ref="RV?"  Part="1" 
F 0 "RV1" H 3080 3346 50  0000 R CNN
F 1 "VOL" H 3080 3255 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 3150 3300 50  0001 C CNN
F 3 "~" H 3150 3300 50  0001 C CNN
	1    3150 3300
	1    0    0    1   
$EndComp
$Comp
L Connector:AudioJack3 J3
U 1 1 5F57069D
P 1950 3100
AR Path="/5F57069D" Ref="J3"  Part="1" 
AR Path="/5F600508/5F57069D" Ref="J?"  Part="1" 
F 0 "J3" H 1932 3425 50  0000 C CNN
F 1 "LineIn" H 1932 3334 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 1950 3100 50  0001 C CNN
F 3 "~" H 1950 3100 50  0001 C CNN
	1    1950 3100
	1    0    0    1   
$EndComp
Wire Wire Line
	4050 3900 3850 3900
Wire Wire Line
	3850 3900 3850 3700
Connection ~ 4550 3900
Wire Wire Line
	4350 3900 4550 3900
Wire Wire Line
	4550 4200 4550 3900
Wire Wire Line
	4700 4350 5000 4350
Wire Wire Line
	5300 4350 5500 4350
Wire Wire Line
	5500 4350 5500 3900
Connection ~ 5500 3900
Wire Wire Line
	4300 5700 4500 5700
Wire Wire Line
	3850 5700 4000 5700
Wire Wire Line
	4500 5950 4500 5700
Wire Wire Line
	5000 6100 4650 6100
Wire Wire Line
	5300 6100 5700 6100
Wire Wire Line
	5700 6100 5700 5700
Connection ~ 5700 5700
Wire Wire Line
	5700 5200 5700 3500
Connection ~ 5700 5200
$Comp
L power:GNDREF #PWR04
U 1 1 5F81F8A5
P 6100 3800
AR Path="/5F81F8A5" Ref="#PWR04"  Part="1" 
AR Path="/5F600508/5F81F8A5" Ref="#PWR?"  Part="1" 
F 0 "#PWR04" H 6100 3550 50  0001 C CNN
F 1 "GNDREF" H 6105 3627 50  0000 C CNN
F 2 "" H 6100 3800 50  0001 C CNN
F 3 "" H 6100 3800 50  0001 C CNN
	1    6100 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3600 6100 3600
Wire Wire Line
	6100 3600 6100 3800
Wire Wire Line
	6300 3500 5700 3500
Wire Wire Line
	5500 3400 6300 3400
Connection ~ 5500 3400
Wire Wire Line
	2250 3200 2150 3200
Wire Wire Line
	2150 3100 2600 3100
Wire Wire Line
	3150 3000 2150 3000
Wire Wire Line
	2600 3100 2600 4950
NoConn ~ 4250 4350
NoConn ~ 4200 6100
Wire Wire Line
	4200 6100 4350 6100
Wire Wire Line
	4250 4350 4400 4350
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5F7ACE4A
P 1050 900
F 0 "J1" H 968 1117 50  0000 C CNN
F 1 "Screw_Terminal" H 968 1026 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2_1x02_P5.00mm_Horizontal" H 1050 900 50  0001 C CNN
F 3 "~" H 1050 900 50  0001 C CNN
	1    1050 900 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3300 1450 3550 1450
Wire Wire Line
	3550 1300 3550 1450
Wire Wire Line
	3550 1550 3300 1550
Wire Wire Line
	3550 1550 3550 1850
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5F7B4A84
P 3100 1450
F 0 "J2" H 3018 1667 50  0000 C CNN
F 1 "Screw_Terminal" H 3018 1576 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2_1x02_P5.00mm_Horizontal" H 3100 1450 50  0001 C CNN
F 3 "~" H 3100 1450 50  0001 C CNN
	1    3100 1450
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
