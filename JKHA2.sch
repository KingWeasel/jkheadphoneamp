EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 5
Title "JK Headphone Amp"
Date "2020-08-24"
Rev "2"
Comp "Line In"
Comment1 "Volume Control and Bulk Highpass"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:OPA2134 U?
U 1 1 5F6402B3
P 6500 3200
AR Path="/5F6402B3" Ref="U?"  Part="1" 
AR Path="/5F600508/5F6402B3" Ref="U2"  Part="1" 
F 0 "U2" H 6500 3567 50  0000 C CNN
F 1 "OPA2134" H 6500 3476 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6500 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 6500 3200 50  0001 C CNN
	1    6500 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3300 6200 3300
Wire Wire Line
	6800 3200 6850 3200
$Comp
L power:GNDREF #PWR?
U 1 1 5F6402BF
P 5500 4100
AR Path="/5F6402BF" Ref="#PWR?"  Part="1" 
AR Path="/5F600508/5F6402BF" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 5500 3850 50  0001 C CNN
F 1 "GNDREF" H 5505 3927 50  0000 C CNN
F 2 "" H 5500 4100 50  0001 C CNN
F 3 "" H 5500 4100 50  0001 C CNN
	1    5500 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4100 5500 4050
Text GLabel 7000 3200 2    50   Input ~ 0
Stage1_Left_Out
Wire Wire Line
	7000 3200 6850 3200
Connection ~ 6850 3200
$Comp
L Device:C C?
U 1 1 5F6402CC
P 5150 5200
AR Path="/5F6402CC" Ref="C?"  Part="1" 
AR Path="/5F600508/5F6402CC" Ref="C6"  Part="1" 
F 0 "C6" H 5265 5246 50  0000 L CNN
F 1 "1u" H 5265 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5188 5050 50  0001 C CNN
F 3 "~" H 5150 5200 50  0001 C CNN
	1    5150 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F6402D2
P 5500 5450
AR Path="/5F6402D2" Ref="R?"  Part="1" 
AR Path="/5F600508/5F6402D2" Ref="R7"  Part="1" 
F 0 "R7" H 5570 5496 50  0000 L CNN
F 1 "100k" H 5570 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5430 5450 50  0001 C CNN
F 3 "~" H 5500 5450 50  0001 C CNN
	1    5500 5450
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA2134 U?
U 2 1 5F6402D8
P 6500 5300
AR Path="/5F6402D8" Ref="U?"  Part="2" 
AR Path="/5F600508/5F6402D8" Ref="U2"  Part="2" 
F 0 "U2" H 6500 5667 50  0000 C CNN
F 1 "OPA2134" H 6500 5576 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 6500 5300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 6500 5300 50  0001 C CNN
	2    6500 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 5300 5500 5200
Wire Wire Line
	6150 5400 6200 5400
Wire Wire Line
	6800 5300 6850 5300
$Comp
L power:GNDREF #PWR?
U 1 1 5F6402E4
P 5500 6200
AR Path="/5F6402E4" Ref="#PWR?"  Part="1" 
AR Path="/5F600508/5F6402E4" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 5500 5950 50  0001 C CNN
F 1 "GNDREF" H 5505 6027 50  0000 C CNN
F 2 "" H 5500 6200 50  0001 C CNN
F 3 "" H 5500 6200 50  0001 C CNN
	1    5500 6200
	1    0    0    -1  
$EndComp
Text GLabel 7000 5300 2    50   Input ~ 0
Stage1_Right_Out
Wire Wire Line
	7000 5300 6850 5300
Connection ~ 6850 5300
Wire Wire Line
	5500 5200 6200 5200
Wire Wire Line
	5300 5200 5500 5200
Connection ~ 5500 5200
Wire Wire Line
	5500 5600 5500 6150
$Comp
L Connector:AudioJack3 J?
U 1 1 5F6402FD
P 3350 2900
AR Path="/5F6402FD" Ref="J?"  Part="1" 
AR Path="/5F600508/5F6402FD" Ref="J3"  Part="1" 
F 0 "J3" H 3332 3225 50  0000 C CNN
F 1 "LineIn" H 3332 3134 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 3350 2900 50  0001 C CNN
F 3 "~" H 3350 2900 50  0001 C CNN
	1    3350 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV?
U 1 1 5F640303
P 4800 3100
AR Path="/5F640303" Ref="RV?"  Part="1" 
AR Path="/5F600508/5F640303" Ref="RV1"  Part="1" 
F 0 "RV1" H 4730 3146 50  0000 R CNN
F 1 "Volume 10k" H 4730 3055 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 4800 3100 50  0001 C CNN
F 3 "~" H 4800 3100 50  0001 C CNN
	1    4800 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_Dual_Separate RV?
U 2 1 5F640309
P 4800 5200
AR Path="/5F640309" Ref="RV?"  Part="2" 
AR Path="/5F600508/5F640309" Ref="RV1"  Part="2" 
F 0 "RV1" H 4730 5246 50  0000 R CNN
F 1 "Volume 10k" H 4730 5155 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK097_Dual_Horizontal" H 4800 5200 50  0001 C CNN
F 3 "~" H 4800 5200 50  0001 C CNN
	2    4800 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2950 4800 2800
Wire Wire Line
	4800 2800 3550 2800
Wire Wire Line
	4800 5050 4800 4600
Wire Wire Line
	4800 4600 3850 4600
Wire Wire Line
	3850 4600 3850 2900
Wire Wire Line
	3850 2900 3550 2900
Wire Wire Line
	3650 5400 3650 3000
Wire Wire Line
	3650 3000 3550 3000
Wire Wire Line
	3650 5400 4800 5400
Connection ~ 4800 5400
Wire Wire Line
	4800 5350 4800 5400
Wire Wire Line
	4950 3100 5000 3100
Wire Wire Line
	4950 5200 5000 5200
Wire Wire Line
	4800 4050 5500 4050
Wire Wire Line
	4800 3250 4800 4050
Connection ~ 5500 4050
Wire Wire Line
	5500 6150 4800 6150
Wire Wire Line
	4800 5400 4800 6150
Connection ~ 5500 6150
Wire Wire Line
	5500 6150 5500 6200
$Comp
L Device:R R?
U 1 1 5F458893
P 6150 3750
AR Path="/5F458893" Ref="R?"  Part="1" 
AR Path="/5F600508/5F458893" Ref="R6"  Part="1" 
F 0 "R6" H 6220 3796 50  0000 L CNN
F 1 "1k" H 6220 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6080 3750 50  0001 C CNN
F 3 "~" H 6150 3750 50  0001 C CNN
	1    6150 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F458EC1
P 6550 3550
AR Path="/5F458EC1" Ref="R?"  Part="1" 
AR Path="/5F600508/5F458EC1" Ref="R5"  Part="1" 
F 0 "R5" H 6620 3596 50  0000 L CNN
F 1 "1k" H 6620 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6480 3550 50  0001 C CNN
F 3 "~" H 6550 3550 50  0001 C CNN
	1    6550 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	6700 3550 6850 3550
Wire Wire Line
	6850 3200 6850 3550
Wire Wire Line
	6400 3550 6150 3550
Wire Wire Line
	6150 3550 6150 3600
Wire Wire Line
	6150 4050 5500 4050
Wire Wire Line
	6150 3900 6150 4050
Wire Wire Line
	6150 3300 6150 3550
Connection ~ 6150 3550
$Comp
L Device:R R?
U 1 1 5F45DC2E
P 6150 5850
AR Path="/5F45DC2E" Ref="R?"  Part="1" 
AR Path="/5F600508/5F45DC2E" Ref="R9"  Part="1" 
F 0 "R9" H 6220 5896 50  0000 L CNN
F 1 "1k" H 6220 5805 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6080 5850 50  0001 C CNN
F 3 "~" H 6150 5850 50  0001 C CNN
	1    6150 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F45DC38
P 6550 5650
AR Path="/5F45DC38" Ref="R?"  Part="1" 
AR Path="/5F600508/5F45DC38" Ref="R8"  Part="1" 
F 0 "R8" H 6620 5696 50  0000 L CNN
F 1 "1k" H 6620 5605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6480 5650 50  0001 C CNN
F 3 "~" H 6550 5650 50  0001 C CNN
	1    6550 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 5650 6150 5650
Wire Wire Line
	6150 5650 6150 5700
Wire Wire Line
	6150 6000 6150 6150
Connection ~ 6150 5650
Wire Wire Line
	6150 5400 6150 5650
Wire Wire Line
	6850 5650 6700 5650
Wire Wire Line
	6850 5300 6850 5650
Wire Wire Line
	5500 6150 6150 6150
Wire Wire Line
	5500 4050 5500 3500
Wire Wire Line
	5300 3100 5500 3100
Wire Wire Line
	5500 3100 6200 3100
Connection ~ 5500 3100
Wire Wire Line
	5500 3200 5500 3100
$Comp
L Device:R R?
U 1 1 5F6402AD
P 5500 3350
AR Path="/5F6402AD" Ref="R?"  Part="1" 
AR Path="/5F600508/5F6402AD" Ref="R4"  Part="1" 
F 0 "R4" H 5570 3396 50  0000 L CNN
F 1 "100k" H 5570 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5430 3350 50  0001 C CNN
F 3 "~" H 5500 3350 50  0001 C CNN
	1    5500 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F6402A7
P 5150 3100
AR Path="/5F6402A7" Ref="C?"  Part="1" 
AR Path="/5F600508/5F6402A7" Ref="C5"  Part="1" 
F 0 "C5" H 5265 3146 50  0000 L CNN
F 1 "1u" H 5265 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5188 2950 50  0001 C CNN
F 3 "~" H 5150 3100 50  0001 C CNN
	1    5150 3100
	0    1    1    0   
$EndComp
Text GLabel 6850 2450 2    50   Input ~ 0
Left_Bulk_Highpass
Text GLabel 7000 4600 2    50   Input ~ 0
Right_Bulk_Highpass
Wire Wire Line
	6850 2450 5500 2450
Wire Wire Line
	5500 2450 5500 3100
Wire Wire Line
	7000 4600 5500 4600
Wire Wire Line
	5500 4600 5500 5200
$EndSCHEMATC
