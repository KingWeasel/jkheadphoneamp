EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 5
Title "JK Headphone Amp"
Date "2020-08-24"
Rev "2"
Comp "Power Supply Section"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5F60EFB9
P 5800 3600
AR Path="/5F60EFB9" Ref="R?"  Part="1" 
AR Path="/5F5FFDB8/5F60EFB9" Ref="R1"  Part="1" 
F 0 "R1" H 5870 3646 50  0000 L CNN
F 1 "10k" H 5870 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5730 3600 50  0001 C CNN
F 3 "~" H 5800 3600 50  0001 C CNN
	1    5800 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3350 5800 3450
$Comp
L Device:C C?
U 1 1 5F60EFF2
P 4450 3800
AR Path="/5F60EFF2" Ref="C?"  Part="1" 
AR Path="/5F5FFDB8/5F60EFF2" Ref="C2"  Part="1" 
F 0 "C2" H 4565 3846 50  0000 L CNN
F 1 "1u" H 4565 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4488 3650 50  0001 C CNN
F 3 "~" H 4450 3800 50  0001 C CNN
	1    4450 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F60EFD1
P 6400 4050
AR Path="/5F60EFD1" Ref="R?"  Part="1" 
AR Path="/5F5FFDB8/5F60EFD1" Ref="R3"  Part="1" 
F 0 "R3" H 6470 4096 50  0000 L CNN
F 1 "10k" H 6470 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6330 4050 50  0001 C CNN
F 3 "~" H 6400 4050 50  0001 C CNN
	1    6400 4050
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7812 U1
U 1 1 5F435783
P 4850 3350
F 0 "U1" H 4850 3592 50  0000 C CNN
F 1 "L7812" H 4850 3501 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4875 3200 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 4850 3300 50  0001 C CNN
	1    4850 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3350 4450 3350
Wire Wire Line
	4450 3650 4450 3350
Wire Wire Line
	4450 3950 4450 4300
$Comp
L Device:C C?
U 1 1 5F60F012
P 5200 3800
AR Path="/5F60F012" Ref="C?"  Part="1" 
AR Path="/5F5FFDB8/5F60F012" Ref="C3"  Part="1" 
F 0 "C3" H 5315 3846 50  0000 L CNN
F 1 "100n" H 5315 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5238 3650 50  0001 C CNN
F 3 "~" H 5200 3800 50  0001 C CNN
	1    5200 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 3650 4850 4300
Connection ~ 4850 4300
Wire Wire Line
	5150 3350 5200 3350
Wire Wire Line
	5200 3350 5200 3650
Wire Wire Line
	5200 3950 5200 4300
Wire Wire Line
	4850 4300 5200 4300
Connection ~ 4450 3350
Connection ~ 4450 4300
Wire Wire Line
	4450 4300 4850 4300
Connection ~ 5200 3350
Connection ~ 5200 4300
Wire Wire Line
	5200 3350 5800 3350
Connection ~ 6400 3350
Wire Wire Line
	6400 4200 6400 4300
Connection ~ 6400 4300
Connection ~ 6750 4300
Connection ~ 8000 4300
Wire Wire Line
	8600 4050 8600 4300
Connection ~ 8000 3350
Wire Wire Line
	8600 3350 8000 3350
Wire Wire Line
	8600 3450 8600 3350
$Comp
L Amplifier_Operational:OPA2134 U?
U 3 1 5F60F039
P 8700 3750
AR Path="/5F60F039" Ref="U?"  Part="3" 
AR Path="/5F5FFDB8/5F60F039" Ref="U3"  Part="3" 
F 0 "U3" H 8658 3796 50  0000 L CNN
F 1 "OPA2134" H 8658 3705 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 8700 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 8700 3750 50  0001 C CNN
	3    8700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3350 8000 3450
Wire Wire Line
	8000 4050 8000 4300
$Comp
L Amplifier_Operational:OPA2134 U?
U 3 1 5F60F02F
P 8100 3750
AR Path="/5F60F02F" Ref="U?"  Part="3" 
AR Path="/5F5FFDB8/5F60F02F" Ref="U2"  Part="3" 
F 0 "U2" H 8058 3796 50  0000 L CNN
F 1 "OPA2134" H 8058 3705 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 8100 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa134.pdf" H 8100 3750 50  0001 C CNN
	3    8100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4400 6750 4300
Connection ~ 6750 3350
Wire Wire Line
	6750 3350 6750 3250
$Comp
L power:VEE #PWR?
U 1 1 5F60F00C
P 6750 4400
AR Path="/5F60F00C" Ref="#PWR?"  Part="1" 
AR Path="/5F5FFDB8/5F60F00C" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 6750 4250 50  0001 C CNN
F 1 "VEE" H 6765 4573 50  0000 C CNN
F 2 "" H 6750 4400 50  0001 C CNN
F 3 "" H 6750 4400 50  0001 C CNN
	1    6750 4400
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5F60EFBF
P 6750 3250
AR Path="/5F60EFBF" Ref="#PWR?"  Part="1" 
AR Path="/5F5FFDB8/5F60EFBF" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 6750 3100 50  0001 C CNN
F 1 "VCC" H 6765 3423 50  0000 C CNN
F 2 "" H 6750 3250 50  0001 C CNN
F 3 "" H 6750 3250 50  0001 C CNN
	1    6750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3450 6400 3350
$Comp
L Device:R R?
U 1 1 5F60EFCB
P 6400 3600
AR Path="/5F60EFCB" Ref="R?"  Part="1" 
AR Path="/5F5FFDB8/5F60EFCB" Ref="R2"  Part="1" 
F 0 "R2" H 6470 3646 50  0000 L CNN
F 1 "10k" H 6470 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6330 3600 50  0001 C CNN
F 3 "~" H 6400 3600 50  0001 C CNN
	1    6400 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4300 6750 4300
Wire Wire Line
	6400 3350 6750 3350
Connection ~ 5800 3350
Wire Wire Line
	5800 3350 6400 3350
Wire Wire Line
	4300 4300 4450 4300
Wire Wire Line
	8000 4300 8600 4300
Wire Wire Line
	5200 4300 5800 4300
Connection ~ 5800 4300
Wire Wire Line
	5800 4300 6400 4300
Wire Wire Line
	6400 3750 6400 3850
Wire Wire Line
	6750 3350 7050 3350
$Comp
L power:GNDREF #PWR?
U 1 1 5F60EFD7
P 7600 3900
AR Path="/5F60EFD7" Ref="#PWR?"  Part="1" 
AR Path="/5F5FFDB8/5F60EFD7" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 7600 3650 50  0001 C CNN
F 1 "GNDREF" H 7605 3727 50  0000 C CNN
F 2 "" H 7600 3900 50  0001 C CNN
F 3 "" H 7600 3900 50  0001 C CNN
	1    7600 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3350 7050 3450
Wire Wire Line
	7050 4200 7050 4300
Connection ~ 7050 3350
Connection ~ 7050 4300
Wire Wire Line
	7050 4300 6750 4300
Wire Wire Line
	7600 3900 7600 3850
Connection ~ 6400 3850
Wire Wire Line
	6400 3850 6400 3900
Wire Wire Line
	7050 3750 7050 3850
Connection ~ 7050 3850
Wire Wire Line
	7050 3850 6400 3850
Wire Wire Line
	7050 3850 7050 3900
Wire Wire Line
	7050 4300 8000 4300
Wire Wire Line
	7050 3350 8000 3350
Wire Wire Line
	7050 3850 7600 3850
$Comp
L Device:CP1 C1
U 1 1 5F4D537D
P 7050 3600
F 0 "C1" H 7165 3646 50  0000 L CNN
F 1 "470u" H 7165 3555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 7050 3600 50  0001 C CNN
F 3 "~" H 7050 3600 50  0001 C CNN
	1    7050 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C4
U 1 1 5F4D5A2A
P 7050 4050
F 0 "C4" H 7165 4096 50  0000 L CNN
F 1 "470u" H 7165 4005 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 7050 4050 50  0001 C CNN
F 3 "~" H 7050 4050 50  0001 C CNN
	1    7050 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3750 5800 3800
Wire Wire Line
	4100 3350 4450 3350
Wire Wire Line
	4100 3550 4300 3550
Wire Wire Line
	4300 3550 4300 4300
$Comp
L Connector:Barrel_Jack J?
U 1 1 5F551B02
P 3800 3450
F 0 "J?" H 3857 3775 50  0000 C CNN
F 1 "Barrel_Jack" H 3857 3684 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-063AH_Horizontal" H 3850 3410 50  0001 C CNN
F 3 "~" H 3850 3410 50  0001 C CNN
	1    3800 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 5F55ECD6
P 5800 3950
F 0 "D?" V 5839 3832 50  0000 R CNN
F 1 "LED" V 5748 3832 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5800 3950 50  0001 C CNN
F 3 "~" H 5800 3950 50  0001 C CNN
	1    5800 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 4100 5800 4300
$EndSCHEMATC
